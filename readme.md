# Rick's PUG HTML builder

A PUG html builder

## How it works

- Everything is kept under the `src` folder
- `emails > index` is where everything is set up
- Call in the parts you need from the parts folder
- You can call parts from another part if needed (see `parts > text-2`)
- CSS is kept in the CSS folder where it will be inlined on build/watch

### Set up

`npm install`

- basic setup

### Commands

`npm run watch`

- deletes old build
- starts browser sync so you can see changes live
- when changes happen it will build a new output
- output to dist as `index.html`
- `ctrl+c` to stop

`npm run build`

- Just builds based on input from index
- output to dist as `index.html`
