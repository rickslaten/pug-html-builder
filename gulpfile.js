const gulp = require('gulp');
const pug = require('gulp-pug');
const inlineCss = require('gulp-inline-css');
const rename = require('gulp-rename');
const del = require('del');
const browserSync = require('browser-sync');

const server = browserSync.create();

// browserSync base directory
// this will be the base directory of files for web preview
// since we are building `index.pug` templates (located in src/emails) to `dist` folder.
function reload(done) {
    server.reload();
    done();
  }

  function serve(done) {
    server.init({
      server: {
        baseDir: 'dist',
      },
    });
    done();
  }

// build complete HTML email template
const build = () => gulp
    // import all email template (name ending with .template.pug) files from src/emails folder
    .src('src/emails/*.pug')

    // compile using Pug
    .pipe(pug())

    // inline CSS
    .pipe(inlineCss({ removeLinkTags: false, removeStyleTags: false }))

    // do not generate sub-folders inside dist folder
    .pipe(rename({ dirname: '' }))

    // put compiled HTML email templates inside dist folder
    .pipe(gulp.dest('dist'));

// delete old for rebuild
const clean = () => del(['dist/*.html']);

// watch source files for changes
// run `build` task when anything inside `src` folder changes
// and reload browserSync
const changes = () => gulp.watch('src/**/*', gulp.series(clean, build, reload));
gulp.task('watch', gulp.series(clean, build, serve, changes));
gulp.task('build', build);
